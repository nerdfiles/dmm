# Degradable Media Models

## _This section is non-normative (descriptive)_. -- HTMLDATE/5

Indeed. 

Perceptual reality, left to human-organic devices suffers the cost of perfection. For a psycho-biological species, maximally performing modules within a closed system emerge at moments where biological imperative no longer records information for survival, but only raw fact, or the brute facts of survivability historically rooted.

One issue is that bureaucratic debt builds along generations, such that the language of a generation loses touch, so to speak, with the political chimaera of a more rapidly developing meme structures within future generations. Legislation allowing laws for certain technologies, for instance, may negate, or subvert, the attempt of 
developing new policies wherein future technologies may open up to expanding sample sizes. Technology introduces complexity, such that a wider number of devices to factor as a contributor to an overall system of city code. Technology itself must tend to itself, so to speak.

### Devices

The idea here is to provide a normative schematization for all future devices, regardless of manufacturer, but rather depending on the perceptual capabilities of the reader in question. In truth, such a system must adapt along the markets spontaneously emerging within any predictable general-purpose network ecology. Resolution should 
never inhibit a general readership from access to information, and within industrial capitalism, historically its strictures of the manufacturing processes have arbitrarily limited access of users to viable information that may serve a multitude of purposes; e.g. the race for cures of advances cancers, [..]

### Browsers

#### What the balls is a Web Browser?

### Perceptual Systems

1. Screen magnifiers
2. Screen readers
3. Text synthesizers
4. Speech recognition
5. Alt-input Technologies
6. Alt-pointing devices

#### Problem Cases

Tabs are a difficult problem in terms of 

    activation, 
    feedback, 
    default state, 
    notification, 
    order, 
    visual modality

But more importantly, such Problem Cases outline Use Cases for a Discovery Layer 
that can be described as Deflationary Orders of Trust insofa as they are 
Distributed Order Executions identity compute-procedures.

### Linguistic Systems

#### Preliminary Notes

1. A font library free of ethnological constraints should be used as the most basic typesetting, for prototypes.
2. Production systems should offer a range of freely available, or open source font libraries.

### Audition Systems

Consider reflections on the HTML5 audio API.
